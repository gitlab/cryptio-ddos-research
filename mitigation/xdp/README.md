# XDP (eXpress Data Path)
## Note
This file was originally taken and adapted from my private XDP Testing repository. The following information is mostly links to XDP resources that are much more well-written and detailed than what I can provide.

## What is it?
- Allows you to attach an eBPF program to a lower-level hook inside the kernel
- Offers high-performance programmable packet processing integrated with the kernel
- eBPF programs are typically written in C, since the LLVM Clang compiler has grown support for an eBPF backend that compiles C into bytecode
- Very fast, allows you to make decisions on packets sooner than netfilter

## Resources
### Packet flow
![Packet flow in Netfilter and General Networking](https://upload.wikimedia.org/wikipedia/commons/3/37/Netfilter-packet-flow.svg "Packet flow")

### Introductions
[Introducing XDP-optimized denial-of-service mitigation](https://www.projectcalico.org/introducing-xdp-optimized-denial-of-service-mitigation/)

[L4Drop: XDP DDoS Mitigations](https://blog.cloudflare.com/l4drop-xdp-ebpf-based-ddos-mitigations/)

[How to drop 10 million packets per second](https://blog.cloudflare.com/how-to-drop-10-million-packets/)

[A practical introduction to XDP](https://linuxplumbersconf.org/event/2/contributions/71/attachments/17/9/presentation-lpc2018-xdp-tutorial.pdf)

[A brief introduction to XDP and eBPF](https://blogs.igalia.com/dpino/2019/01/07/a-brief-introduction-to-xdp-and-ebpf/)

### Practice
[xdp-tutorial](https://github.com/xdp-project/xdp-tutorial)

### Advanced
[XDP use-case: DDoS scrubber](https://prototype-kernel.readthedocs.io/en/latest/networking/XDP/use-cases/xdp_use_case_ddos_scrubber.html)

[Hello XDP_DROP](https://www.netronome.com/blog/hello-xdp_drop/)

[Achieving high-performance, low-latency networking with XDP: Part I](https://developers.redhat.com/blog/2018/12/06/achieving-high-performance-low-latency-networking-with-xdp-part-1/)


### Compiling and loading BPF
[xdp-tutorial](https://github.com/xdp-project/xdp-tutorial/tree/master/basic01-xdp-pass/README.org)

### Printing from an eBPF program
[xdp-tutorial](https://github.com/xdp-project/xdp-tutorial/tree/master/tracing03-xdp-debug-print)

Monitoring tracing:

```bash
watch -d  tail /sys/kernel/debug/tracing/trace
```


## Monitoring XDP
First, build `xdp_loader` and your eBPF program which has a map, storing each action that was taken. Next, you can copy `xdp_loader` to the same directory as your eBPF program and run this:

```bash
./xdp_loader -d interfacename --filename bpffilename.o -S --force
```

where interfacename is the name of the network interface you wish to load the eBPF program to, and bpffilename.o is the compiled eBPF bytecode.

If it succeeds, then you should be able to run `xdp_stats `and monitor the stats per XDP action (i.e XDP_DROP):
```bash
$ ./xdp_status -d interfacename
Collecting stats from BPF map
 - BPF map (bpf_map_type:6) id:380 name:xdp_stats_map key_size:4 value_size:16 max_entries:5
XDP-action  
XDP_ABORTED            0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:0.250322
XDP_DROP               0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:0.250299
XDP_PASS             198 pkts (         4 pps)          19 Kbytes (     0 Mbits/s) period:0.250290
XDP_TX                 0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:0.250292
XDP_REDIRECT           0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:0.250308
```

## Restrictions
- The maximum instruction limit per program is restricted to 4096 BPF instructions
- The BPF verifier ensures that BPF programs are safe (i.e they will not crash the system, access invalid memory addresses, etc). If the program is deemed unsafe, it is not loaded.
- While the Linux kernel 5.3 introduced support for loops, it did not appear as if this was the case on Ubuntu 20.04 LTS using kernel version 5.4.0-28-generic, nor 5.5.0-0.bpo.2-cloud-amd64 on Debian 10. The observed behavior was that the XDP loader was not able to pin maps if you accessed past a certain element number in an array during a for loop. Before the verifier had support for loops, `#pragma unroll` could be placed immediately before a bounded loop. This directs the compiler to unroll the loop when it sees it. The unroll directive is currently still used in this repository's code.


## Future desires
- Turing completeness (support for unbounded loops such as while loops). This will let us do things like use binary searches for arrays instead of linear searching.
