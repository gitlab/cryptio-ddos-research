# Monitoring
A list of tools and resources to help you monitor network traffic and other system resources. These tools can be used during stress testing to see how an attack impacts your services.

## CLI
### nload
nload is a console application for Linux which monitors and visualizes realtime network traffic and bandwidth usage, as well as keeping track of statistics such as peak and average values.

![nload](https://alvinalexander.com/sites/default/files/styles/preview/public/photos/linux-nload-network-traffic-graphs.jpg)

### dstat
dstat allows you to view all of your system resources instantly and monitor the usage. It is very powerful and commonly used.

![dstat](https://www.dedoimedo.com/images/computers-years/2019-1/dstat-cpu-disk-metrics.png)

## Web interfaces
### netdata
[netdata](https://www.netdata.cloud/) allows you to monitor system health metrics from a sleek web interface. It includes alarms for when charts read abnormal values.

Features:
- Supports for a plethora of collectors. The most interesting components for stress testing include the CPU usage graph, conntrack and SYNPROXY chart, and charts for the inbound and outbound packets per second (PPS)
- Can be configured to fire off alarms to custom destinations
- [Proxied netdata setup with master/slave layout](https://learn.netdata.cloud/docs/agent/streaming/) provides you with a centralized dashboard for all of your servers where you can switch between them easily to see their netdata.

![netdata](https://img2.helpnetsecurity.com/posts/netdata.jpg)