# KILL-ALL
## Preface
This patch was originally private, but because of greedy people who like to profit from someone else's work and claim it as their own, it had to be publicized before it got too far. So for everyone who purchased a setup from me which fixed this attack, I apologize that a few bad apples had to ruin it for everyone. If you see anyone trying to sell my work for a ridiculous price, please call them out on stealing my patch!

## What is it?
- Self-awarded Most Annoying Attack of the Year
- TCP reflection attack
- Originally named DRDoS SYN. This is because the attack uses TCP reflection, hence the "R" for "reflection"
- Sold to a DDoS-for-hire service called SecurityTeam.io and renamed as KILL-ALL

## How does it work?
For information on how TCP reflection works, please refer to [my TCP reflection writeup](https://gitlab.com/cryptio/ddos-research/-/blob/master/attacks/tcp-reflection.md). The issue with this DDoS attack method is how hosting providers such as NFO and OVH try to mitigate it. These mitigation providers are unable to distinguish between unsolicited SYN-ACK packets and replies to previously initiated outbound TCP connections. The result is new outbound TCP connections failing when an attack happens. This is why, despite this attack having no real impact on the server's resources, the DDoS still succeeds in denying service to legitimate users. Thus, the core issue lies at the infrastructure-level.

## Effectiveness
- Unlike popular UDP based amplification attacks from services which are clearly not needed and can thus be safely blocked, we cannot straight up block KILL-ALL unless we don't want our server to be able to send/receive web traffic
- Currently, the affected hosting providers either refuse to fix it or don't care enough to look into it
- Not a lot of traffic is required to trigger the mitigation and block out legitimate TCP traffic. We have only observed attacks between 100Mbps and 1Gbps being capable of getting the job done

## Affected mitigation systems
- NFO - while they can detect attacks such as SYN floods, their DDoS defense system is based on iptables and doesn't seem to use connection tracking to offer stateful mitigation. Instead, one can only filter based on TCP flags
- OVH - despite having a pre firewall that allows you to filter out new vs established traffic, it does not appear as if their firewall is completely stateful. VAC struggles to mitigate KILL-ALL without dropping legitimate traffic
- Magic Transit - As of writing this, flowtrackd has not been deployed to all customers, which is essentially an eBPF/XDP implementation of conntrack that only tracks inbound connections. This is because Magic Transit only scrubs ingress traffic. The lack of stateful mitigation in Magic Transit makes it very easy to saturate the host with TCP reflection, as well as other TCP based attacks such as SYN and ACK floods

## Unaffected mitigation systems
- Path - Path Network has deployed a completely stateful mitigation system capable of handling SYN, SYN-ACK, and ACK floods without blocking legitimate traffic, meaning that KILL-ALL will not impact the server

## Mitigation
### Addressing this bug to the afflicted hosting providers
The best fix for this issue is making hosting providers aware of it. This way, it can be fixed for everyone as opposed to each client having to attempt to mitigate it by themselves, especially when it is a method like this which you do not have much control over.

### Separating web traffic from all other traffic
From my coverage on TCP reflection, we know that the most common reflectors happen to be HTTP and HTTPS servers. Other TCP services such as SMTP could be used for TCP reflection, but it is mainly webservers. We also know that this attack craps on attempts to establish outbound TCP connections, since the SYN-ACK packet never arrives due to OVH or NFO's mitigation. Since I am the most familiar with OVH's infrastructure, I will be covering how to fix it for OVH servers.
It should be noted that OVH's anti-DDoS system functions on a per-IP basis, meaning different IP addresses that are all routing their traffic to one service can have different mitigation states and Access Control Lists (ACLs) simultaneously.

#### Ordering an additional IP address
You may obtain a secondary IP address from OVH if you navigate to the "IP" tab and hit "Order additional IPs". Make sure you order the location which corresponds to the server you want to attach it to. You obviously can't have a Canadian server with a Canada IP address, but a secondary IP from France attached to it. Once it has been ordered, make sure it's linked to the right service.

#### Configuring the system
```bash
# Get primary NIC device name
export PRIMARY_NIC=$(route | grep '^default' | grep -o '[^ ]*$')
export SECONDARY_IP=yoursecondaryip
# Don't change this unless you already have more than one IP address configured which is taking up the label name ${PRIMARY-NIC}:1 (i.e eth0:1)
export ALT_LABEL=1

# Disable source address verification
sudo sysctl -w net.ipv4.conf."${PRIMARY_NIC}".rp_filter=0

# i.e PRIMARY_NIC=eth0, ALT_LABEL=1
# secondary IP label=eth01
echo "200 ${PRIMARY_NIC}${ALT_LABEL}-route" >> /etc/iproute2/rt_tables

# Add secondary IP address
ip addr add "${SECONDARY_IP}"/32 broadcast "${SECONDARY_IP}" dev "${PRIMARY_NIC}" label "${PRIMARY_NIC}":"${ALT_LABEL}"

# Populate secondary routing table
ip route add default via "${SECONDARY_IP}" dev "${PRIMARY_NIC}":"${ALT_LABEL}" table "${PRIMARY_NIC}""${ALT_LABEL}"-route

# Packets marked with 0x(alt_label) will be mapped to the alternate route
ip rule add fwmark 0x"${ALT_LABEL}" table "${PRIMARY_NIC}""${ALT_LABEL}"-route

# Mark these packets so that iproute can route it through the secondary route
iptables -A OUTPUT -t mangle -o "${PRIMARY_NIC}" -p tcp -m multiport --dports 80,443 -j MARK --set-mark "${ALT_LABEL}"
# Rewrite the source IP address
iptables -A POSTROUTING -t nat -o "${PRIMARY_NIC}" -p tcp -m multiport --dports 80,443 -j SNAT --to "${SECONDARY_IP}"
```

#### Configuring the network firewall
Now that you have your two IP addresses, with one pushing all HTTP and HTTPS traffic to the secondary IP, you can block direct inbound web traffic from the primary IP address, and configure the secondary one so it only accepts that.

For OVH, it would be something like this:

Failover IP address Pre Firewall rules:

| Action | Priority | Protocol | Source Port | Destination Port | TCP Flags |
|--|--|--|--|--|--|
| Authorize | 18 | TCP |  |  |ESTABLISHED |
| Refuse | 19 | IPv4 |  |  | |

Main IP address Pre Firewall rules (put your own allowance rules between #2 and #18):

| Action | Priority | Protocol | Source Port | Destination Port | TCP Flags |
|--|--|--|--|--|--|
| Refuse | 0 | TCP | 80 | | |
| Refuse | 1 | TCP | 443 | | |
| Refuse | 19 | IPv4 |  |  | |

#### Result
The end result should be that attackers cannot DDoS your server using KILL-ALL through its main IP address. The secondary IP address should be kept private, as it is the only one that can handle web traffic. In the event that you do, for some dumb reason, give out the failover IP address, web activities such as watching videos or browsing may lag a little bit, but you will not be disconnected from video games or anything as KILL-ALL usually does.

#### Notes
- Since the secondary IP address is accepting TCP traffic, you are still vulnerable to resource saturation from TCP reflection attacks. With that being said, you should still be taking the necessary efforts to mitigate these types of floods in the event that they ever made it through. You can learn about how to [here](tcp-reflection.md)
- This setup works for VPN traffic. It was tested on OpenVPN, where traffic from the clients is getting forwarded to the primary interface, and then web traffic is specifically being rerouted to the secondary IP address. Users were able to load websites and use the server without any issues. Despite this, it appears as if it may cause issues for other outbound web traffic. The workaround is whitelisting the traffic you need exempted from being routed to the secondary IP address before the `MARK` rule like this:
```bash
iptables -I INPUT -p tcp -s example.com -j ACCEPT
iptables -I OUTPUT -p tcp -d example.com -j ACCEPT
```
